package Pet;

import java.time.LocalDateTime;
/**
 * class that deals with dog object
 * @author judem
 *
 */
public class Dog extends Pets{
	private boolean canBark;
	/**
	 * constructor for the dog object
	 * @param petRef pet reference number
	 * @param petName pet name
	 * @param speciesName species name
	 * @param regDate registration date
	 * @param breed breed of animal
	 * @param canBark boolean that stores whether the pet can bark
	 */
	public Dog(String petRef, String petName, String speciesName, LocalDateTime regDate, String breed, boolean canBark) {
		super(petRef, petName, speciesName, regDate, breed);
		this.canBark = canBark;
	}
	
	//Dont know if getters and setters are necessary but im making them for convenience
	
	/**
	 * getter for whether the pet object can bark
	 * @return canBark the boolean which stores whether the pet can bark
	 */
	public boolean canItBark() {
		return canBark;
	}
	/**
	 * sets the canBark variable for an object
	 * @param canBark boolean which stores whether it barks
	 */
	public void setCanBark(boolean canBark) {
		this.canBark = canBark;
	}

}
