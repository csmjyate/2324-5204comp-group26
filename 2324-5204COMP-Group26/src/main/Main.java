package main;


import java.util.ArrayList;
import java.util.HashSet;
import java.util.Scanner;

import Booking.ManageBookings;
import Pet.ManagePets;
import Surgery.SurgeriesMenu;
import Staff.AvailabilityCheck;
//import Staff.ManageStaff;
import Staff.Staff;



/**
 * Main class of the program.
 * @author Jack Yates
 */
public class Main {
	
    public static final Scanner SCAN = new Scanner(System.in);
    
    /**
     * Main method of the project; this will send the user straight into the menu.
     * @param args Arguments passed into the program
     */
    public static void main(String[] args) {
        Main mainInstance = new Main(); 
        mainInstance.menu(); 
    }

    /**
     * This method outputs the menu to the user and calls the relevant method relating to their input.
     * @author jacky
     */  
	public void menu() {
	
		SurgeriesMenu surgeries = new SurgeriesMenu(); 
		Staff staff = new Staff(null, null, null, 0, null, 0);
		ManagePets managePets = new ManagePets();
		ManageBookings manageBookings = new ManageBookings();
		AvailabilityCheck availabilityCheck = new AvailabilityCheck();
		
		String input = "";
		while (!input.equals("Q")) {
			System.out.println("\n---Welcome to the Veterinary Management System---");
			System.out.println("A. Surgeries");
			System.out.println("B. Staff");
			System.out.println("C. Pets");
			System.out.println("D. Bookings");
			System.out.println("E. Availability Check");
			System.out.println("Q. Quit");
			System.out.print("Please enter a menu selection: ");
			input = SCAN.next().toUpperCase();
			switch (input) {
			case "A" -> surgeries.surgeriesMenu(); // Surgeries
			case "B" -> staff.staffMenu(); // Staff
			case "C" ->   managePets.petBookingMenu(); // Pets
			case "D" -> manageBookings.bookingsMenu(); // Bookings
			case "E" -> availabilityCheck.main(null);     //Checking Availability
			case "F" -> SCAN.close();
			default -> System.out.printf("%nSorry! This input is not valid! Please try again!%n"); 
			}
		}
	}
}

