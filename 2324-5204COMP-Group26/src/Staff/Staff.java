package Staff;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import Booking.ManageBookings;
import Surgery.Surgeries;
import Surgery.ManagingSurgeryStaff;
import main.Main;



public class Staff {
	
	private final String PATH = "C:\\Users\\jacky\\OneDrive - Liverpool John Moores University\\Glynn Group Coursework\\data";
	
	private List<Staff> staff = new ArrayList<Staff>();
	
    private String staffRef;
    private String name;
    private String typeOfStaff;
    private int noOfBookings;
    private String surgeryID;
    private int noOfSurgeries;

    public Staff(String staffRef, String name, String typeOfStaff, int noOfBookings, String surgeryID, int noOfSurgeries) {
        this.staffRef = staffRef;
        this.name = name;
        this.typeOfStaff = typeOfStaff;
        this.noOfBookings = noOfBookings;
        this.surgeryID = surgeryID;
        this.noOfSurgeries = noOfSurgeries;
    }

    // Getters and setters for each field

    public String getStaffRef() {
        return staffRef;
    }

    public void setStaffRef(String staffRef) {
        this.staffRef = staffRef;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTypeOfStaff() {
        return typeOfStaff;
    }

    public void setTypeOfStaff(String typeOfStaff) {
        this.typeOfStaff = typeOfStaff;
    }

    public int getNoOfBookings() {
        return noOfBookings;
    }

    public void setNoOfBookings(int noOfBookings) {
        this.noOfBookings = noOfBookings;
    }

    public String getSurgeryID() {
        return surgeryID;
    }

    public void setSurgeryID(String surgeryID) {
        this.surgeryID = surgeryID;
    }

    public int getNoOfSurgeries() {
        return noOfSurgeries;
    }

    public void setNoOfSurgeries(int noOfSurgeries) {
        this.noOfSurgeries = noOfSurgeries;
    }


	static final Scanner SCAN = new Scanner(System.in);

	public void staffMenu() {
	ManageBookings manageBooking = new ManageBookings();
	Staff staff = new Staff(name, name, name, noOfBookings, name, noOfBookings);
		System.out.println("What is your role? (Nurse or Surgeon)");
		String role = SCAN.nextLine().toUpperCase();
		String input = "";
		  switch(role) {
		  case "NURSE" -> staff.nurseMenu();
		  case "SURGEON" -> staff.surgeonMenu();
		  }
			
	  }  public void surgeonMenu() {
		  ManageBookings manageBooking = new ManageBookings();
		  String input = "";
			while (!input.equals("Q")) {
				System.out.println("---Surgeon Menu---");
				System.out.println("A. Add Booking");
				System.out.println("B. Remove Booking");
				System.out.println("C. Search Booking");
				System.out.println("D. View All Bookings");
				System.out.println("E. Availability Check");  
				System.out.println("F. Add Consulting Surgery");            // Menu
				System.out.println("Q. Quit Surgeon Menu");
				System.out.print("Please enter a menu selection: ");
				
				input = SCAN.next().toUpperCase();
				switch (input) {
				case "A" -> manageBooking.addBookings();
				case "B" -> manageBooking.cancelBookings();
				case "C" -> manageBooking.searchBookings();
				case "D" -> manageBooking.viewAllBookings();
				//case "E" -> AvailabilityCheck.availabilityCheck(staffDataList);  //not complete
				case "F" -> System.out.println("This still needs developed"); //link to adding consulting surgeries
				case "Q" -> System.out.println("Exiting Staff...");
				default -> System.out.printf("%nSorry! This input is not valid! Please try again!%n");
				}
	    	}
	  }  
	  
	public void nurseMenu() {
		ManageBookings manageBooking = new ManageBookings();
		String input = SCAN.nextLine();
		while (!input.equals("Q")) {
			System.out.println("---Nurse Menu---");
			System.out.println("A. Add Booking");
			System.out.println("B. Remove Booking");
			System.out.println("C. Search Booking");
			System.out.println("D. View All Bookings");
			System.out.println("E. Availability Check");                   // Menu
			System.out.println("Q. Quit Nurse Menu");
			System.out.print("Please enter a menu selection: ");
			
			input = SCAN.next().toUpperCase();
			switch (input) {
			case "A" -> manageBooking.addBookings();
			case "B" -> manageBooking.cancelBookings();
			case "C" -> manageBooking.searchBookings();
			case "D" -> manageBooking.viewAllBookings();
			//case "E" -> AvailabilityCheck.availabilityCheck(); //not complete
			case "Q" -> System.out.println("Exiting Staff...");
			default -> System.out.printf("%nSorry! This input is not valid! Please try again!%n");
			}
    	}
	 }
	
	private void deSerialize() {
		ObjectInputStream ois;

		try {
			ois = new ObjectInputStream(new FileInputStream(PATH + "c&s.ser"));

			// NOTE : Erasure Warning !
			staff = (ArrayList<Staff>)ois.readObject();

			// ToDo : Finally ?
			ois.close();
		}
		catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	private void serialize() {
		ObjectOutputStream oos;

		try {
			oos = new ObjectOutputStream(new FileOutputStream(PATH + "c&s.ser"));
			oos.writeObject(staff);

			// ToDo : Finally ?
			oos.close();
		}
		catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
}