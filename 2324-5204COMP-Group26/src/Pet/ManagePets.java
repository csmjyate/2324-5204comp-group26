package Pet;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.UUID;

import Booking.Bookings;
import Booking.ManageBookings;
/**
 * class that deals with managing all pets data and operations
 * @author judem
 *
 */
public class ManagePets {
	
	 private static final String PETS_CSV_PATH = "C:\\Users\\jacky\\OneDrive - Liverpool John Moores University\\Glynn Group Coursework\\csvFiles\\pets.csv";
	 private static final String BOOKINGS_CSV_PATH = "C:\\Users\\jacky\\OneDrive - Liverpool John Moores University\\Glynn Group Coursework\\csvFiles\\bookings.csv";
	 private static final String SURGERIES_CSV_PATH = "C:\\Users\\jacky\\OneDrive - Liverpool John Moores University\\Glynn Group Coursework\\csvFiles\\surgery.csv";
	 
	static final Scanner SCAN = new Scanner(System.in);
	/**
	 * Menu for pets
	 */
	public void petBookingMenu() {
        Scanner scanner = new Scanner(System.in);
        String input = "";
        while (!input.equals("Q")) {
            System.out.println("---Pet Booking Operations---");
            System.out.println("A. Add Booking");
            System.out.println("B. Remove Booking");
            System.out.println("C. Next Booking");
            System.out.println("D. View All Bookings");
            System.out.println("Q. Quit");
            System.out.print("Please enter a menu selection: ");

            input = scanner.next().toUpperCase();
            switch (input) {
                case "A" -> addBookingByPet();
                case "B" -> removeBookingByPet();
                case "C" -> nextBooking(null);
                case "D" -> viewAllBookingsOfPet();
                case "Q" -> System.out.println("Exiting Pet Booking Menu...");
                default -> System.out.println("Invalid input. Please try again!");
            }
        }
    }
/**
 * Asks the user all necessary information for it then to be stored in our array and file
 */
	public void addBookingByPet() {
		ManageBookings manageBookings = new ManageBookings();
		// Display available pets for booking
//		displayAvailablePets(petDataList);
		
	
		System.out.print("Enter the reference number of the pet for booking: ");
		String petID = SCAN.nextLine();
		
	
		System.out.print("Enter staff ID: ");
		String staffID = SCAN.nextLine();
		
		
		System.out.print("Enter surgery ID: ");
		String surgeryID = SCAN.nextLine();
		
	
		System.out.print("Enter start date and time (dd/MM/yyyy HH:MM): ");
		String startDateTimeStr = SCAN.nextLine();
		LocalDateTime startDateTime = LocalDateTime.parse(startDateTimeStr, DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm"));
		
		
		System.out.print("Enter end date and time (dd/MM/yyyy HH:MM): ");
		String endDateTimeStr = SCAN.nextLine();
		LocalDateTime endDateTime = LocalDateTime.parse(endDateTimeStr, DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm"));
	
		if (checkSurgeryForPet(surgeryID, petID)) {
			// Add booking to the list or CSV file
			UUID uuid = UUID.randomUUID();
			Bookings newBooking = new Bookings(uuid, petID, startDateTime, endDateTime, staffID);
            manageBookings.serializeBookings(newBooking); // Serialise after adding a Booking
            System.out.println("Bookings added successfully. Bookings ID: " + uuid);
		} else {
			System.out.println("Pet or surgery does not exist in the same record. Booking cannot be created.");
		}
	}
	
//	private void displayAvailablePets() {
//		System.out.println("Available pets for booking:");
//		for (Pets pet : petDataList) {
//			System.out.println("Pet ID: " + pet.getPetRef() + ", Name: " + pet.getPetName());
//		}
//	}
	
	/**
	 * checks the surgery to see if a pet exists in the array
	 * @param surgeryID ID for the surgery being searched
	 * @param petID ID for the pet being checked
	 * @return
	 */
	private boolean checkSurgeryForPet(String surgeryID, String petID) { //not developed
		return false;

	}
/**
 * outputs all bookings stored in the array
 * @return nothing because the viewAllBookings method returns the data
 */
	public void viewAllBookingsOfPet() {
		ManageBookings manageBookings = new ManageBookings();
		manageBookings.viewAllBookings();
		return;
	}
/**
 * This method outputs the next booking from a provided current booking by the user
 * @param deserializeAllBookings this is the list of all the bookings
 * @return the next booking or nothing if one doesn't exist
 */
	public void nextBooking(ArrayList<Bookings> deserializeAllBookings) {
		Scanner scanner = new Scanner(System.in);
		ManageBookings booking = new ManageBookings();
		System.out.println("Enter your last booking start time (dd/MM/yyyy HH:mm): ");
		LocalDateTime lastBookingStartTime = LocalDateTime.parse(scanner.next());
		Bookings nextBooking = findNextBooking(booking.deserializeAllBookings(), lastBookingStartTime);
		if (nextBooking != null) {
			System.out.println("Next booking: ");
			System.out.println("Booking UUID: " + nextBooking.getId());
			System.out.println("Start Date: " +nextBooking.getStartDateTime());
			System.out.println("End Date: " + nextBooking.getEndDateTime());
			System.out.println("Staff ID: " + nextBooking.getStaffID());
			System.out.println("Pet ID: " + nextBooking.getPetID());
		}else {
			System.out.println("No next booking found.");
		}
		return;	
		}
	/**
	 * this method finds the booking which follows the current booking that the user inputted
	 * @param deserializeAllBookings the array that holds all data
	 * @param lastBookingStartTime the user inputted current booking starting time
	 * @return the next booking after the current one
	 */
    private static Bookings findNextBooking(ArrayList<Bookings> deserializeAllBookings, LocalDateTime lastBookingStartTime) {
        for (Bookings booking : deserializeAllBookings) {
            if (booking.getStartDateTime().isAfter(lastBookingStartTime)) {
                return booking;
            }
        }
        return null; // Return null if no next booking found
    }
	

	public Object removeBookingByPet() {		//not developed
		// TODO Auto-generated method stub 
		return null;
	}
	
}
