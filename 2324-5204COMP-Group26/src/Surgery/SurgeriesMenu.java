package Surgery;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import main.Main;

/**
 * This is the Surgeries menu which acts as the main hub of the Surgeries package. 
 * @author jacky
 *
 */
public class SurgeriesMenu {

	private static final String PATH = "C:\\Users\\jacky\\OneDrive - Liverpool John Moores University\\csvFiles\surgery.csv";

//	private static List<Surgeries> surgeries = new ArrayList<Surgeries>();
	

	static final Scanner SCAN = new Scanner(System.in);

	/**
	 * This is the main menu for the Surgerys package.
	 */
	public void surgeriesMenu() {

		ManagingSurgeryStaff staffManager = new ManagingSurgeryStaff();
		ManagingSurgeryBookings bookingManager = new ManagingSurgeryBookings();
		ManagingSurgeryPets petsManager = new ManagingSurgeryPets();

		String input = "";
		while (!input.equals("Q")) {
			System.out.println("---Surgeries---");
			System.out.println("A. Manage Staff");
			System.out.println("B. Manage Bookings");
			System.out.println("C. Manage Pets"); // Menu
			System.out.println("Q. Quit Surgeries");
			System.out.print("Please enter a menu selection: ");

			input = Main.SCAN.next().toUpperCase();
			switch (input) {
			case "A" -> staffManager.manageStaff();
			case "B" -> bookingManager.manageBookings();
			case "C" -> petsManager.managePets();
			case "Q" -> System.out.println("Exiting Surgeries...");
			default -> System.out.printf("%nSorry! This input is not valid! Please try again!%n");
			}
		}

	}
}
