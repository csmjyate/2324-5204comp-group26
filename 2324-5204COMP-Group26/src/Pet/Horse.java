package Pet;

import java.time.LocalDateTime;
/**
 * class that deals with horse object
 * @author judem
 *
 */
public class Horse extends Pets {
	private boolean canRun;
	/**
	 * constructor for horse object
	 * @param petRef pet reference
	 * @param petName pet name
	 * @param speciesName species name
	 * @param regDate registration date
	 * @param breed breed of animal
	 * @param canRun boolean for whether the animal can run or not
	 */
	public Horse(String petRef, String petName, String speciesName, LocalDateTime regDate,String breed, boolean canRun) {
		super(petRef, petName, speciesName, regDate, breed);
		this.canRun = canRun;
	}
	
	//Dont know if getters and setters are necessary but im making them for convenience
	/**
	 * getter for whether the animal can run
	 * @return canRun boolean which stores whether it can run or not
	 */
	public boolean canItRun() {
		return canRun;
	}
	/**
	 * sets the canRun variable
	 * @param canRun boolean which stores whether the animal can run or not
	 */
	public void setCanRun(boolean canRun) {
		this.canRun = canRun;
	}

}
