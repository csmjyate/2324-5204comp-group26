package Staff;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.UUID;

import Booking.Bookings;
import main.Main;

/**
 * The AvailabilityCheck class provides methods for checking the availability of staff members based on their bookings.
 */
/**
 * class that deals with checking if a staff member is available or not
 * @author judem
 *
 */
public class AvailabilityCheck {
    
    private static final String BOOKINGS_CSV_PATH = "C:\\Users\\jacky\\git\\2324-5204comp-group26\\2324-5204COMP-Group26\\csvFiles\\bookings.csv";
    private static final Scanner scanner = new Scanner(System.in);

    
    public void main(String[] args) {
        AvailabilityCheck availabilityCheck = new AvailabilityCheck(); 
        availabilityCheck.availabilityCheck(); 
    }
    
/**
 * \ This method is used to deserialise the bookings from the bookings.csv file stored in the csvFiles folder.
 * @author jacky
 */
    public List<Bookings> deserializeBookings() {
        List<Bookings> bookings = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(BOOKINGS_CSV_PATH))) {
            String line;
            boolean isFirstLine = true; // Flag to skip the first line (header)
            while ((line = br.readLine()) != null) {
                if (isFirstLine) {
                    isFirstLine = false;
                    continue; 
                }
                String[] fields = line.split(",");
                UUID bookingID = UUID.fromString(fields[0].trim()); 
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm");
                LocalDateTime startTime = LocalDateTime.parse(fields[1].trim(), formatter);
                LocalDateTime endTime = LocalDateTime.parse(fields[2].trim(), formatter);
                String staffID = fields[3].trim(); 
                String petID = fields[4].trim(); 
                String surgeryID = fields[5].trim(); 
                
                Bookings booking = new Bookings(bookingID, startTime, endTime, staffID, petID, surgeryID);
                bookings.add(booking);
            }
        } catch (IOException e) {
            System.err.println("Error reading bookings data: " + e.getMessage());
        }
        return bookings;
    }
    
    /**
     * This method is used to check the availability of the staff members using the staffID and the startDateTime and endDateTime of a booking.
     * @param staffID this is passed into the method to look through all the available times for that staff member.
     * @param desiredStartTime this is passed in as a users input, it is then calculated whether the staffMember is free.
     */
    private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm");

    public void availabilityCheck() {
        System.out.print("Enter Staff ID: ");
        String staffID = scanner.nextLine();

        System.out.print("Enter Desired Start Time (dd/MM/yyyy HH:mm): ");
        String timeInput = scanner.nextLine();
        LocalDateTime desiredStartTime = LocalDateTime.parse(timeInput, formatter);

        List<Bookings> bookings = deserializeBookings();
        for (Bookings booking : bookings) {
            if (booking.getStaffID().equals(staffID)) {
                LocalDateTime startTime = booking.getStartDateTime();
                LocalDateTime endTime = booking.getEndDateTime();

                if (startTime != null && endTime != null &&
                    desiredStartTime.isAfter(startTime) && desiredStartTime.isBefore(endTime)) {
                    System.out.println("Staff member is not available at the desired start time.");
                    return;
                }
            }
        }
        System.out.println("Staff member is available at the desired start time.");
    }
}