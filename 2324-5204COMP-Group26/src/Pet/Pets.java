package Pet;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import Staff.Staff;
import Surgery.Surgeries;
import main.Main;
/** 
 The Pets class shows the information about pets, including their pet references,
names, species, registration dates, and breeds which are being saved in the data.
 */

public class Pets {
	
private final String PATH = "C:\\Users\\jacky\\OneDrive - Liverpool John Moores University\\Glynn Group Coursework\\data";
	
	private List<Pets> pets = new ArrayList<Pets>();
	
	private String petRef;
	private String petName;
	private String speciesName;
	private LocalDateTime regDate;
	private String breed;
	
	public Pets (String petRef, String petName, String speciesName, LocalDateTime regDate, String breed) {
		this.petRef = petRef;
		this.petName = petName;
		this.speciesName = speciesName;
		this.regDate = regDate;
		this.breed = breed;
	}
	
	/**
	 * Takes the breed of the pet as input from user.
     *
     * @return The breed of the pet.
     */
	public String getBreed() {
		// Getter code
		return breed;
	}
	
	/**
     * Gets the Reference code for pet.
     *
     * @return The reference code of the pet.
     */

	public String getPetRef() {
		return petRef;
	}
	
	/**
     * Gets the name of the pet.
     *
     * @return The name of the pet.
     */
	public String getPetName() {
		return petName;
	}
	
	 /**
     * Gets the specie name of the pet.
     *
     * @return The specie name of pet.
     */ 
	public String getSpeciesName() {
		return speciesName;
	}
	
	/**
     * Gets the registration date of the pet.
     *
     * @return Registration date of the pet.
     */
	public LocalDateTime getregDate() {
		return regDate;
	}
	
	/**
     * Sets the petReference.
     *
     * @param petRef The new reference code for the pet.
     */

	public void setPetRef(String petRef) {
		this.petRef = petRef;
	}
	
	/**
     * Sets the name of the pet.
     *
     * @param petName The new name for the pet.
     */
	
	public void setPetName(String petName) {
		this.petName = petName;
	}
	
	 /**
     * Set the  new speicies name 
     *
     * @param speciesName this is the specie name for the pet .
     */
	public void setSpeciesName(String speciesName) {
		this.speciesName = speciesName;
	}
	
	/**
     * Set the registratuon date for the pet.
     *
     * @param regDate This is the registration date of the pet.
     */
	public void setRegDate(LocalDateTime regDate) {
		this.regDate = regDate;
	}
	
	private void deSerialize() {
		ObjectInputStream ois;

		try {
			ois = new ObjectInputStream(new FileInputStream(PATH + "c&s.ser"));

			// NOTE : Erasure Warning !
			pets = (ArrayList<Pets>)ois.readObject();

			// ToDo : Finally ?
			ois.close();
		}
		catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	private void serialize() {
		ObjectOutputStream oos;

		try {
			oos = new ObjectOutputStream(new FileOutputStream(PATH + "c&s.ser"));
			oos.writeObject(pets);

			// ToDo : Finally ?
			oos.close();
		}
		catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
	
}




	
	
