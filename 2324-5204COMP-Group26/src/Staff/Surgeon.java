package Staff;

/**
 * The Surgeon class represents surgeons in thesystem, extending the Staff class.
 * It inherits attributes such as staff reference, name, type of staff, number of bookings,
 * surgery ID, and number of surgeries.
 * The class also includes a method for handling
 * consulting surgeries.
 */
public class Surgeon extends Staff {
	
	public Surgeon (String staffRef, String name, String typeOfStaff, int noOfBookings, String surgeryID, int noOfSurgeries) {
		super(staffRef, name, typeOfStaff, noOfBookings, surgeryID, noOfSurgeries);
	}

	//making this void temporarily until we decide how this works
	public void consultingSurgeries() {
		System.out.println("Placeholder");
	}
	
}
