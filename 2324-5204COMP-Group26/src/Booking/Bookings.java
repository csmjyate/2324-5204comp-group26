package Booking;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.UUID;
/**
 * class that stores all booking variables
 * @author judem
 *
 */
public class Bookings {
    private UUID id;
    private LocalDateTime startDateTime;
    private LocalDateTime endDateTime;
    private String staffID;
    private String petID;
    private String surgeryID;
    
/**
 * constructor for bookings object
 * @param id booking ID
 * @param startTime start time of booking
 * @param endTime end time of booking
 * @param staffID staff ID
 * @param petID pet ID
 * @param surgeryID surgery ID
 */
    public Bookings(UUID id, LocalDateTime startTime, LocalDateTime endTime, String staffID, String petID, String surgeryID) {
        this.id = id;
        this.startDateTime = startDateTime;
        this.endDateTime = endDateTime;
        this.staffID = staffID;
        this.petID = petID;
        this.surgeryID = surgeryID;
        
    }
/**
 * another constructor for a different bookings object used elsewhere
 * @param uuid booking ID
 * @param petID pet ID
 * @param startDateTime start time of booking
 * @param endDateTime end time of booking
 * @param staffID staff ID
 */
    public Bookings(UUID uuid, String petID, LocalDateTime startDateTime, LocalDateTime endDateTime, String staffID) {
		this.petID = petID;
		this.startDateTime = startDateTime;
		this.endDateTime = endDateTime;
		this.staffID = staffID;
		
				
	}
/**
 * getter for booking ID
 * @return id ID for booking
 */
	public UUID getId() {
        return id;
    }
/**
 * sets ID for booking
 * @param id booking ID
 */
    public void setId(UUID id) {
        this.id = id;
    }
/**
 * gets booking start time
 * @return startDateTime start time variable
 */
    public LocalDateTime getStartDateTime() {
        return startDateTime;
    }
/**
 * sets start time variable
 * @param startDateTime start time variable
 */
    public void setStartDateTime(LocalDateTime startDateTime) {
        this.startDateTime = startDateTime;
    }
/**
 * gets end time of booking
 * @return endDateTime variable for end time
 */
    public LocalDateTime getEndDateTime() {
        return endDateTime;
    }
/**
 * sets end time variable
 * @param endDateTime end time variable
 */
    public void setEndDateTime(LocalDateTime endDateTime) {
        this.endDateTime = endDateTime;
    }
/**
 * gets staff ID
 * @return staffID 
 */
    public String getStaffID() {
        return staffID;
    }
/**
 * sets staff ID variable
 * @param staffID
 */
    public void setStaffID(String staffID) {
        this.staffID = staffID;
    }
/**
 * gets pet ID
 * @return petID
 */
    public String getPetID() {
        return petID;
    }
/**
 * sets pet ID variable
 * @param petID
 */
    public void setPetID(String petID) {
        this.petID = petID;
    }
    /**
     * gets surgery ID
     * @return surgeryID
     */
    public String getSurgeryID() {
        return surgeryID;
    }
/**
 * sets surgery ID variable
 * @param surgeryID
 */
    public void setSurgeryID(String surgeryID) {
        this.surgeryID = surgeryID;
    }
}






	
	
	
//previous development...
	
	
//	public static void addBooking() {
//		Scanner scanner = new Scanner(System.in);
//		
//		System.out.println("Enter booking start date and time (DD - MMM - YYYY - TIME): ");
//		String startDateTimeStr = scanner.nextLine();
//		
//		System.out.println("Enter a booking end date and time (DD - MMM - YYYY - TIME): ");
//		String endDateTimeStr = scanner.nextLine();
//		
//		LocalDateTime startDateTime = LocalDateTime.parse(startDateTimeStr, formatter);
//		LocalDateTime endDateTime = LocalDateTime.parse(endDateTimeStr, formatter);
//		
//		if (hasConflictedBooking(startDateTime, endDateTime)) {
//			System.out.println("There is conflicting time slots with an existing booking please try a different time.");
//			return; //exits the method early so the user can't add a booking when it conflicts with another
//		}
//		UUID uuid = UUID.randomUUID();
//		
//		Bookings newBooking = new Bookings(uuid, startDateTime, endDateTime);
//		bookings.add(newBooking);
//	}
//
//	private static boolean hasConflictedBooking(LocalDateTime startDateTime, LocalDateTime endDateTime) {
//		for (Bookings booking : bookings) {
//			if ((startDateTime.isAfter(booking.getstartDateTime()) && startDateTime.isBefore(booking.getEndDateTime()))|| 
//				(endDateTime.isAfter(booking.getstartDateTime()) && (endDateTime.isBefore(booking.getEndDateTime())) || 
//				(startDateTime.isBefore(booking.getstartDateTime()) && (endDateTime.isAfter(booking.getEndDateTime())) ||		
//				(startDateTime.isEqual(booking.getstartDateTime()) || endDateTime.isEqual(booking.getEndDateTime()))))); {
//					return true; //bookings have conflicted
//				}
//		}
//		return false;
//	}
//	
//	private static void viewAllBookings() {
//		if (bookings.isEmpty()) {
//			System.out.println("No Bookings Found");
//		}else {
//			System.out.println("All Current Bookings: ");
//			for (Bookings booking : bookings) {
//				System.out.println(booking); //after some research, when trying to print an object, java automatically called the toString() method,
//			}                                //because of this, it calls our overridden toString() method and prints necessary data
//		}
//	}
	
	


