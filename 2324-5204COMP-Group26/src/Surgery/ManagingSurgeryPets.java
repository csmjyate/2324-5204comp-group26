package Surgery;

import java.io.*;
import java.util.*;

import main.Main;

/**
 * This is the class for managing pets assigned to a certain surgery.
 * @author jacky
 *
 */
public class ManagingSurgeryPets {

    private static final String PETS_CSV_PATH = "C:\\path\\to\\pets.csv"; 
    private static final String SURGERY_CSV_PATH = "C:\\path\\to\\surgery.csv"; 
    private static final Scanner SCAN = new Scanner(System.in);

    
    /**
     * This is the menu to manage the pets for a specific surgery.
     */
    void managePets() {

		String input = "";
		while (!input.equals("Q")) {
			System.out.println("---Managing Surgeries Pets---");
			System.out.println("A. Add Pet");
			System.out.println("B. Remove Pet");
			System.out.println("C. Manage Pets"); 
			System.out.println("Q. Quit Surgeries");
			System.out.print("Please enter a menu selection: ");

			input = Main.SCAN.next().toUpperCase();
			switch (input) {
			case "A" -> addPet();
			case "B" -> removePet();
			case "C" -> searchPets();
			case "Q" -> System.out.println("Exiting Surgeries...");
			default -> System.out.printf("%nSorry! This input is not valid! Please try again!%n");
			}
		}

	}

/**
 * This is the method to add a pet for a specific surgery.
 */
    public void addPet() {
        System.out.println("Enter the Surgery ID:");
        String surgeryID = SCAN.nextLine().trim();
        
       
        if (!surgeryExists(surgeryID)) {
            System.out.println("Error: Surgery with ID " + surgeryID + " does not exist.");
            return;
        }
        
        System.out.println("Enter pet details:");
        System.out.print("Pet Reference: ");
        String petRef = SCAN.nextLine();
        System.out.print("Pet Name: ");
        String petName = SCAN.nextLine();
        System.out.print("Species Name: ");
        String speciesName = SCAN.nextLine();
        System.out.print("Breed: ");
        String breed = SCAN.nextLine();
        System.out.print("Registration Date: ");
        String regDate = SCAN.nextLine();
        System.out.print("Can Bark? (true/false): ");
        boolean canBark = Boolean.parseBoolean(SCAN.nextLine());
        System.out.print("Can Swim? (true/false): ");
        boolean canSwim = Boolean.parseBoolean(SCAN.nextLine());
        System.out.print("Can Fly? (true/false): ");
        boolean canFly = Boolean.parseBoolean(SCAN.nextLine());

        Pet newPet = new Pet(petRef, petName, speciesName, breed, regDate, canBark, canSwim, canFly, surgeryID);

        List<Pet> petList = deserialisePets();
        petList.add(newPet);
        savePets(petList); 

        System.out.println("Pet added successfully.");
    }

	/**
	 * This is the method to validate whether if the surgery exists in the surgery
	 * CSV, then the user can proceed with their operation.
	 * 
	 * @param surgeryID
	 * @return
	 */
    private boolean surgeryExists(String surgeryID) {
        try (BufferedReader reader = new BufferedReader(new FileReader(SURGERY_CSV_PATH))) {
            String line;
            while ((line = reader.readLine()) != null) {
                String[] parts = line.split(",");
                if (parts.length >= 1 && parts[0].trim().equalsIgnoreCase(surgeryID)) {
                    return true;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }
    
    
    /**
     * This is the method to search for pets in the surgery.csv, the user must provide the surgery they are from first.
     */
    public void searchPets() {
        System.out.println("Enter the Surgery ID:");
        String surgeryID = SCAN.nextLine().trim();

       
        if (!surgeryExists(surgeryID)) {
            System.out.println("Surgery not found.");
            return;
        }

    
        List<Pet> petList = deserialisePets();

 
        System.out.println("Enter the pet reference you want to search:");
        String petRef = SCAN.nextLine().trim();

   
        Pet foundPet = findPetByReference(petList, surgeryID, petRef);
        if (foundPet != null) {
            System.out.println("Pet found:");
            System.out.println(foundPet);
        } else {
            System.out.println("No pet found for the specified surgery and pet reference.");
        }
    }
    
	/**
	 * This is the method to fins a pet by reference, using the csv loading all the
	 * pets into an array, to find the petRef with that surgeryID
	 * 
	 * @param petList
	 * @param surgeryID
	 * @param petRef
	 * @return
	 */

	private Pet findPetByReference(List<Pet> petList, String surgeryID, String petRef) {
		for (Pet pet : petList) {
			if (pet.getSurgeryID().equalsIgnoreCase(surgeryID) && pet.getPetRef().equalsIgnoreCase(petRef)) {
				return pet;
			}
		}
		return null;
    }
    
	
	/**
	 * This is the method we used to deserialise the pets.csv file and load the data into a list format.
	 * @return
	 */
    public List<Pet> deserialisePets() {
        List<Pet> petList = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(PETS_CSV_PATH))) {
            String line;
            while ((line = br.readLine()) != null) {
                String[] data = line.split(",");
                if (data.length >= 9) {
                    Pet pet = new Pet(data[0], data[1], data[2], data[3], data[4],
                                      Boolean.parseBoolean(data[5]), Boolean.parseBoolean(data[6]),
                                      Boolean.parseBoolean(data[7]), data[8]);
                    petList.add(pet);
                } else {
                    System.err.println("Invalid data in CSV: " + line);
                }
            }
        } catch (IOException e) {
            System.err.println("Error loading pets: " + e.getMessage());
        }
        return petList;
    }
    
    /**
     * THis is the method to remove a pet from a surgery given they provide the surgeryID they are from.
     */
    public void removePet() {
        System.out.println("Enter the Surgery ID:");
        String surgeryID = SCAN.nextLine().trim();

        System.out.println("Enter the Pet Reference of the pet you want to remove:");
        String petRef = SCAN.nextLine().trim();

   
        List<Pet> petList = deserialisePets();

        Optional<Pet> optionalPet = petList.stream()
                .filter(pet -> pet.getSurgeryID().equals(surgeryID) && pet.getPetRef().equals(petRef))
                .findFirst();

        if (optionalPet.isPresent()) {
            Pet petToRemove = optionalPet.get();
            petList.remove(petToRemove);
            savePets(petList); 
            System.out.println("Pet with Pet Reference " + petRef + " has been removed.");
        } else {
            System.out.println("Pet with Pet Reference " + petRef + " not found in Surgery with ID " + surgeryID + ".");
        }
    }
    
	/**
	 * This is the method to save the array of pet list into the pets.csv so that
	 * upon launch of the applicatio, it will load the updated data
	 * 
	 * @param petList
	 */
	public void savePets(List<Pet> petList) {
		try (PrintWriter writer = new PrintWriter(new FileWriter(PETS_CSV_PATH))) {
			for (Pet pet : petList) {
				writer.println(pet.toCSV());
			}
		} catch (IOException e) {
			System.err.println("Error saving pets: " + e.getMessage());
		}
	}

	
	
	/**
	 * This is the pets class which stores the attributes and the get and set methods of the attributes into the class.
	 * @author jacky
	 *
	 */
    public class Pet implements Serializable {
        private String petRef;
        private String petName;
        private String speciesName;
        private String breed;
        private String regDate;
        private boolean canBark;
        private boolean canSwim;
        private boolean canFly;
        private String surgeryID;

        public Pet(String petRef, String petName, String speciesName, String breed, String regDate, boolean canBark, boolean canSwim, boolean canFly, String surgeryID) {
            this.petRef = petRef;
            this.petName = petName;
            this.speciesName = speciesName;
            this.breed = breed;
            this.regDate = regDate;
            this.canBark = canBark;
            this.canSwim = canSwim;
            this.canFly = canFly;
            this.surgeryID = surgeryID;
        }

        public String toCSV() {
            return String.join(",", petRef, petName, speciesName, breed, regDate, String.valueOf(canBark), String.valueOf(canSwim), String.valueOf(canFly), surgeryID);
        }

        /**
         * This converts the pet record into a string.
         */
        @Override
        public String toString() {
            return "Pet{" +
                    "petRef='" + petRef + '\'' +
                    ", petName='" + petName + '\'' +
                    ", speciesName='" + speciesName + '\'' +
                    ", breed='" + breed + '\'' +
                    ", regDate='" + regDate + '\'' +
                    ", canBark=" + canBark +
                    ", canSwim=" + canSwim +
                    ", canFly=" + canFly +
                    ", surgeryID='" + surgeryID + '\'' +
                    '}';
        }

        
        /**
         * Get and set methods for the pet class.
         * @return
         */
        public String getPetRef() {
            return petRef;
        }

        public void setPetRef(String petRef) {
            this.petRef = petRef;
        }

        public String getPetName() {
            return petName;
        }

        public void setPetName(String petName) {
            this.petName = petName;
        }

        public String getSpeciesName() {
            return speciesName;
        }

        public void setSpeciesName(String speciesName) {
            this.speciesName = speciesName;
        }

        public String getBreed() {
            return breed;
        }

        public void setBreed(String breed) {
            this.breed = breed;
        }

        public String getRegDate() {
            return regDate;
        }

        public void setRegDate(String regDate) {
            this.regDate = regDate;
        }

        public boolean isCanBark() {
            return canBark;
        }

        public void setCanBark(boolean canBark) {
            this.canBark = canBark;
        }

        public boolean isCanSwim() {
            return canSwim;
        }

        public void setCanSwim(boolean canSwim) {
            this.canSwim = canSwim;
        }

        public boolean isCanFly() {
            return canFly;
        }

        public void setCanFly(boolean canFly) {
            this.canFly = canFly;
        }

        public String getSurgeryID() {
            return surgeryID;
        }

        public void setSurgeryID(String surgeryID) {
            this.surgeryID = surgeryID;
        }
    }

 
    }


