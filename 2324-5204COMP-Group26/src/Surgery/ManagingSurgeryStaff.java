package Surgery;

import java.io.*;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.*;
import Staff.Staff;
import main.Main;

/**
 * This is the class which manages the Surgery Staff and the operations
 * performed on the data of staff from a certain surgery.
 * 
 * @author jacky
 *
 */
public class ManagingSurgeryStaff {

	private static final String PATH = "M:\\data\\OOSyS\\";
	
    private static final String STAFF_CSV_PATH = "C:\\Users\\jacky\\OneDrive - Liverpool John Moores University\\Glynn Group Coursework\\csvFiles\\staff.csv";
    private static final String SURGERY_CSV_PATH = "C:\\Users\\jacky\\OneDrive - Liverpool John Moores University\\Glynn Group Coursework\\csvFiles\\surgery.csv";
    private static final Scanner SCAN = new Scanner(System.in);

    
    /**
     * This is the main menu.
     */
	void manageStaff() {

		String input = "";
		while (!input.equals("Q")) {
			System.out.println("---Manage Surgeries Staff---");
			System.out.println("A. Add Staff");
			System.out.println("B. Remove Staff");
			System.out.println("C. Manage Staff"); // Menu
			System.out.println("Q. Surgeries Menu");
			System.out.print("Please enter a menu selection: ");

			input = Main.SCAN.next().toUpperCase();
			switch (input) {
			case "A" -> addStaff();
			case "B" -> removeStaff();
			case "C" -> searchStaff();
			case "Q" -> System.out.println("Exiting Surgeries...");
			default -> System.out.printf("%nSorry! This input is not valid! Please try again!%n");
			}
		}

	}
	
	/**
	 * THis is the way we have deserialised our csv file to implement the functions
	 * of adding, removing and searching staff. We then load the csv into array
	 * format
	 * 
	 * @return
	 */
	public List<Staff> deserialiseStaff() {
		List<Staff> staffList = new ArrayList<>();
		try (BufferedReader br = new BufferedReader(new FileReader(STAFF_CSV_PATH))) {
			String line;
			boolean isFirstLine = true; // Flag to skip the first line (header)
			while ((line = br.readLine()) != null) {
				if (isFirstLine) {
					isFirstLine = false;
					continue;
				}
				String[] fields = line.split(",");
				String staffID = fields[0].trim();
				String staffName = fields[1].trim();
				String typeOfStaff = fields[2].trim();
				int noOfBookings = Integer.parseInt(fields[3].trim());
				String surgeryID = fields[4].trim();
				int noOfSurgeries = Integer.parseInt(fields[5].trim());

				Staff staff = new Staff(staffID, staffName, typeOfStaff, noOfBookings, surgeryID, noOfSurgeries);
				staffList.add(staff);
			}
		} catch (IOException e) {
			System.err.println("Error reading staff data: " + e.getMessage());
		}
		return staffList;
	}

	/**
	 * THis is the way we have deserialised our csv file to implement the functions
	 * of adding, removing and searching staff. We then load the csv into array
	 * format
	 * 
	 * @return
	 */
	    public List<Surgeries> deserialiseSurgeries() {
	        List<Surgeries> surgeriesList = new ArrayList<>();
	        try (BufferedReader br = new BufferedReader(new FileReader(SURGERY_CSV_PATH))) {
	            String line;
	            boolean isFirstLine = true; // Flag to skip the first line (header)
	            while ((line = br.readLine()) != null) {
	                if (isFirstLine) {
	                    isFirstLine = false;
	                    continue;
	                }
	                String[] fields = line.split(",");
	                String surgeryID = fields[0].trim();
	                String name = fields[1].trim();
	                String postcode = fields[2].trim();
	                LocalTime openingTime = LocalTime.parse(fields[3].trim());
	                LocalTime closingTime = LocalTime.parse(fields[4].trim());
	                LocalDateTime trainingDay = LocalDateTime.parse(fields[4].trim());

	                Surgeries surgery = new Surgeries(surgeryID, name, openingTime, closingTime, trainingDay);
	                surgeriesList.add(surgery);
	            }
	        } catch (IOException e) {
	            System.err.println("Error reading surgeries data: " + e.getMessage());
	        }
	        return surgeriesList;
	    }
	
	
/**
 * This is the method to add a staff member to a certain surgery
 */
    public void addStaff() {
        System.out.println("Enter staff details:");
        System.out.print("Staff ID: ");
        String staffID = SCAN.nextLine();
        System.out.print("Name: ");
        String name = SCAN.nextLine();
        String typeOfStaff;
        while (true) {
            System.out.print("Type of Staff (Nurse - N / Surgeon - S): ");
            String input = SCAN.nextLine().toUpperCase();
            if (input.equals("N")) {
                typeOfStaff = "Nurse";
                break;
            } else if (input.equals("S")) {
                typeOfStaff = "Surgeon";
                break;
            } else {
                System.out.println("Invalid input. Please enter 'N' for Nurse or 'S' for Surgeon.");
            }
        }
        System.out.print("Number of Bookings: ");
        int noOfBookings = Integer.parseInt(SCAN.nextLine());
        System.out.print("Surgery ID: ");
        String surgeryID = SCAN.nextLine();
        System.out.print("Number of Surgeries: ");
        int noOfSurgeries = Integer.parseInt(SCAN.nextLine());

        Staff newStaff = new Staff(staffID, name, typeOfStaff, noOfBookings, surgeryID, noOfSurgeries);

   
        List<Staff> staffList = deserialiseStaff();
        staffList.add(newStaff);

  
        saveStaff(staffList);

        System.out.println("Staff member added successfully.");
    }


    /**
     * This is the method to remove a staff member from a certain surgery. 
     */
    public void removeStaff() {
        System.out.println("Enter the ID of the staff member you want to remove:");
        String staffRef = SCAN.nextLine();

       
        List<Staff> staffList = deserialiseStaff();

        Optional<Staff> optionalStaff = staffList.stream()
                .filter(staff -> staff.staffUUID.equals(staffRef))
                .findFirst();

        if (optionalStaff.isPresent()) {
            Staff staffToRemove = optionalStaff.get();
            staffList.remove(staffToRemove);
            saveStaff(staffList); 
            System.out.println("Staff member with ID " + staffRef + " has been removed.");
        } else {
            System.out.println("Staff member with ID " + staffRef + " not found.");
        }
    }

	/**
	 * This is the save staff methodd which will save the operations performed on
	 * the csv so that when the file is reopened then it will save record.
	 * 
	 * @param staffList
	 */
	public void saveStaff(List<Staff> staffList) {
		try (PrintWriter writer = new PrintWriter(new FileWriter(STAFF_CSV_PATH))) {
			for (Staff staff : staffList) {
				writer.println(staff.toCSV());
			}
		} catch (IOException e) {
			System.err.println("Error saving staff members: " + e.getMessage());
		}
	}
	
	
	/**
	 * This is the searchStaff operation which allows a user to only search for a
	 * staff member if they are from that surgery.
	 */
    public void searchStaff() {
        System.out.println("Enter the ID of the surgery you are from:");
        String surgeryID = SCAN.nextLine().trim();

        if (!surgeryExists(surgeryID)) {
            System.out.println("Surgery not found.");
            return;
        }

        List<Staff> staffList = deserialiseStaff();

    
        System.out.println("Enter the name of the staff member you want to search:");
        String staffName = SCAN.nextLine().trim();

       
        Staff staffMember = findStaffMember(staffList, surgeryID, staffName);
        if (staffMember != null) {
            System.out.println("Staff member found:");
            System.out.println(staffMember);
        } else {
            System.out.println("Staff member not found.");
        }
    }


    
    /**
	 * This is the checking to see if surgery Exists for validation in the
	 * operations we are performing. This asks for a surgeryID they are from, and if
	 * that surgery Exists then they can perform operations.
	 * 
	 * @param surgeryID
	 * @return
	 */
    private boolean surgeryExists(String surgeryID) {
        try (BufferedReader reader = new BufferedReader(new FileReader(SURGERY_CSV_PATH))) {
            String line;
            boolean isFirstLine = true; 
            while ((line = reader.readLine()) != null) {
                if (isFirstLine) {
                    isFirstLine = false;
                    continue;
                }
                String[] parts = line.split(",");
            
                String idFromCsv = parts[0].trim();
                if (idFromCsv.equalsIgnoreCase(surgeryID.trim())) {
                    return true;
                }
            }
        } catch (IOException e) {
            e.printStackTrace(); 
        }
        return false;
    }

    
    /**
     * This class allows for the finding of staff members if they have the same surgeryID and staffName in the same column of staff.
     * @param staffList
     * @param surgeryID
     * @param staffName
     * @return
     */
    private Staff findStaffMember(List<Staff> staffList, String surgeryID, String staffName) {
        for (Staff staff : staffList) {
            if (staff.getSurgeryID().equalsIgnoreCase(surgeryID) && staff.getName().equalsIgnoreCase(staffName)) {
                return staff;
            }
        }
        return null; 
    }
    
/**
 * THis is the staff class to load the attributes and help the csv deserializer to work.
 * @author jacky
 *
 */
    public class Staff implements Serializable {
        private String staffUUID;
        private String name;
        private String surgeryID;
        private String typeOfStaff;
        private int noOfBookings;
        private int noOfSurgeries;

        public Staff(String staffUUID, String name, String typeOfStaff, int noOfBookings, String surgeryID, int noOfSurgeries) {
            this.staffUUID = staffUUID;
            this.name = name;
            this.typeOfStaff = typeOfStaff;
            this.noOfBookings = noOfBookings;
            this.surgeryID = surgeryID;
            this.noOfSurgeries = noOfSurgeries;
        }

        public String toCSV() {
            return String.join(",", staffUUID, name, typeOfStaff, String.valueOf(noOfBookings), surgeryID, String.valueOf(noOfSurgeries));
        }

        @Override
        public String toString() {
            return "Staff{" +
                    "staffUUID='" + staffUUID + '\'' +
                    ", name='" + name + '\'' +
                    ", surgeryID='" + surgeryID + '\'' +
                    ", typeOfStaff='" + typeOfStaff + '\'' +
                    ", noOfBookings=" + noOfBookings +
                    ", noOfSurgeries=" + noOfSurgeries +
                    '}';
        }

       /**
        * THe get and set methods for the attributes of the staff.
        * @return
        */
        public String getTypeOfStaff() {
            return typeOfStaff;
        }

        public void setTypeOfStaff(String typeOfStaff) {
            this.typeOfStaff = typeOfStaff;
        }

        public int getNoOfBookings() {
            return noOfBookings;
        }

        public void setNoOfBookings(int noOfBookings) {
            this.noOfBookings = noOfBookings;
        }

        public String getSurgeryID() {
            return surgeryID;
        }

        public void setSurgeryID(String surgeryID) {
            this.surgeryID = surgeryID;
        }

        public int getNoOfSurgeries() {
            return noOfSurgeries;
        }

        public void setNoOfSurgeries(int noOfSurgeries) {
            this.noOfSurgeries = noOfSurgeries;
        }
        
        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }
        
}




