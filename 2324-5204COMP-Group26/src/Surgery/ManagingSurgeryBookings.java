package Surgery;
import java.io.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

import Booking.Bookings;
import Booking.ManageBookings;
import main.Main;

/**
 * This class manages the operations of bookings for only the surgery the user is from.
 * @author jacky
 *
 */
public class ManagingSurgeryBookings {

	private static final String BOOKINGS_CSV_PATH = "C:\\Users\\jacky\\OneDrive - Liverpool John Moores University\\Glynn Group Coursework\\csvFiles\\bookings.csv";
    private static final Scanner SCAN = new Scanner(System.in);


    /**
     * This is the Menu for managing bookings for a surgery.
     */
	void manageBookings() {

		ManageBookings manageBooking = new ManageBookings();

		String input = "";
		while (!input.equals("Q")) {
			System.out.println("---Managing Surgeries Bookings---");
			System.out.println("A. Make Booking");
			System.out.println("B. Cancel Booking");
			System.out.println("C. Search Booking"); // Menu
			System.out.println("Q. Quit Surgeries");
			System.out.print("Please enter a menu selection: ");

			input = Main.SCAN.next().toUpperCase();
			switch (input) {
			case "A" -> surgeryAddBooking();
//			case "B" -> cancelBooking();
			case "C" -> viewBookings();
			case "Q" -> System.out.println("Exiting Surgeries...");
			default -> System.out.printf("%nSorry! This input is not valid! Please try again!%n");
			}
		}

	}
	
	/**
	 * This method directs the user to the add booking in the Booking Package.
	 */
    public void surgeryAddBooking() {
    	ManageBookings manageBooking = new ManageBookings();  	
    	manageBooking.addBookings();
    }
/**Used the add booking in the bookings class.
  /*  public static void addBooking() {
        System.out.println("Enter booking details:");
        System.out.print("Start Time (YYYY-MM-DD HH:mm): ");
        String startTime = SCAN.nextLine();
        System.out.print("End Time (YYYY-MM-DD HH:mm): ");
        String endTime = SCAN.nextLine();
        System.out.print("Staff ID: ");
        String staffID = SCAN.nextLine();
        System.out.print("Pet ID: ");
        String petID = SCAN.nextLine();
        System.out.print("Surgery ID: ");
        String surgeryID = SCAN.nextLine();

        // Generate UUID for the booking
        String bookingUUID = UUID.randomUUID().toString();
        
        Booking newBooking = new Booking(bookingUUID, startTime, endTime, staffID, petID, surgeryID);

        try (FileWriter writer = new FileWriter(BOOKINGS_CSV_PATH, true);
             BufferedWriter bw = new BufferedWriter(writer);
             PrintWriter out = new PrintWriter(bw)) {
            out.println(newBooking.toCSV());
            System.out.println("Booking added successfully.");
        } catch (IOException e) {
            System.err.println("Error adding booking: " + e.getMessage());
        }
        viewBookings();
    }*/

    /**
     * This method cancels a booking upon the p
     */
    
    /**
     * To be developed.
     * @return
     */
//    public void cancelBooking() {
//        System.out.println("Enter the UUID of the booking you want to cancel:");
//        String uuid = SCAN.nextLine();
//        
//        // Load existing bookings
//        List<Bookings> bookings = deserialiseBookings();
//        
//        Optional<Booking> optionalBooking = bookings.stream()
//                .filter(booking -> booking.bookingUUID.equals(uuid))
//                .findFirst();
//        
//        if (optionalBooking.isPresent()) {
//            Booking bookingToRemove = optionalBooking.get();
//            bookings.remove(bookingToRemove);
//            saveBookings(bookings); // Save updated bookings to file
//            System.out.println("Booking with UUID " + uuid + " has been cancelled.");
//        } else {
//            System.out.println("Booking with UUID " + uuid + " not found.");
//        }
//    }

    /**
     * this method loads the bookings into an array and 
     * @return
     */
    public List<Booking> loadBookings() {
        List<Booking> bookings = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(BOOKINGS_CSV_PATH))) {
            String line;
            while ((line = br.readLine()) != null) {
                String[] data = line.split(",");
                if (data.length >= 6) {
                    Booking booking = new Booking(data[0], data[1], data[2], data[3], data[4], data[5]);
                    bookings.add(booking);
                } else {
                    System.err.println("Invalid data in CSV: " + line);
                }
            }
        } catch (IOException e) {
            System.err.println("Error loading bookings: " + e.getMessage());
        }
        return bookings;
    }
    
   /** 
    * This is the method to save a booking when it is made for when the application is loaded up again.
    * @param bookings
    */
    public void saveBookings(List<Booking> bookings) {
        try (PrintWriter writer = new PrintWriter(new FileWriter(BOOKINGS_CSV_PATH))) {
            for (Booking booking : bookings) {
                writer.println(booking.toCSV());
            }
        } catch (IOException e) {
            System.err.println("Error saving bookings: " + e.getMessage());
        }
    }
    
    /**
     * This method outputs all of the bookings
     * @return
     */
    public List<Booking> viewBookings() {
        List<Booking> bookings = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(BOOKINGS_CSV_PATH))) {
            String line;
            while ((line = br.readLine()) != null) {
            	String[] data = line.split(",");
            	if (data.length >= 6) {
            	    Booking booking = new Booking(data[0], data[1], data[2], data[3], data[4], data[5]);
            	    bookings.add(booking);
            	} else {
            	    System.err.println("Invalid data in CSV: " + line);
            	}
            }
        } catch (IOException e) {
            System.err.println("Error searching bookings: " + e.getMessage());
        }

        System.out.println("All bookings:");
        for (Booking booking : bookings) {
            System.out.println(booking);
        }

        return bookings;
    }
    
    public List<Bookings> deserialiseBookings() {
        List<Bookings> bookings = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(BOOKINGS_CSV_PATH))) {
            String line;
            boolean isFirstLine = true; // Flag to skip the first line (header)
            while ((line = br.readLine()) != null) {
                if (isFirstLine) {
                    isFirstLine = false;
                    continue; 
                }
                String[] fields = line.split(",");
                UUID bookingID = UUID.fromString(fields[0].trim()); 
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm");
                LocalDateTime startTime = LocalDateTime.parse(fields[1].trim(), formatter);
                LocalDateTime endTime = LocalDateTime.parse(fields[2].trim(), formatter);
                String staffID = fields[3].trim(); 
                String petID = fields[4].trim(); 
                String surgeryID = fields[5].trim(); 
                
                Bookings booking = new Bookings(bookingID, startTime, endTime, staffID, petID, surgeryID);
                bookings.add(booking);
            }
        } catch (IOException e) {
            System.err.println("Error reading bookings data: " + e.getMessage());
        }
        return bookings;
    }

    public class Booking implements Serializable {
        private String bookingUUID;
        private String startTime;
        private String endTime;
        private String staffID;
        private String petID;
        private String surgeryID;

        public Booking(String bookingUUID, String startTime, String endTime, String staffID, String petID, String surgeryID) {
            this.bookingUUID = bookingUUID;
            this.startTime = startTime;
            this.endTime = endTime;
            this.staffID = staffID;
            this.petID = petID;
            this.surgeryID = surgeryID;
        }


		public String toCSV() {
            return String.join(",", bookingUUID, startTime, endTime, staffID, petID, surgeryID);
        }

        @Override
        public String toString() {
            return "Booking{" +
                    "bookingUUID: " + bookingUUID + '\'' +
                    ", startTime: " + startTime + '\'' +
                    ", endTime: " + endTime + '\'' +
                    ", staffID: " + staffID + '\'' +
                    ", petID: " + petID + '\'' +
                    ", surgeryID: " + surgeryID + '\'' +
                    '}';
        }
    }
}

