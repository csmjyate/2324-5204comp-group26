package Pet;

import java.time.LocalDateTime;
/**
 * class that deals with bird object
 * @author judem
 *
 */
public class Hawk extends Pets{
	private boolean canFly;
	/**
	 * constructor for hawk object
	 * @param petRef pet reference number
	 * @param petName pet name
	 * @param speciesName species of animal
	 * @param regDate registration date
	 * @param breed breed of animal
	 * @param canFly boolean for whether the animal can fly
	 */
	public Hawk(String petRef, String petName, String speciesName, LocalDateTime regDate,String breed, boolean canFly) {
		super(petRef, petName, speciesName, regDate, breed);
		this.canFly = canFly;
	}

	//Dont know if getters and setters are necessary but im making them for convenience
	/**
	 * getter for whether the animal can fly
	 * @return canFly boolean that stores whether it flies of not
	 */
	public boolean canItFly() {
		return canFly;
	}
	/**
	 * sets the canFly variable for an object
	 * @param canFly boolean that stores whether it flies of not
	 */
	public void setCanFly(boolean canFly) {
		this.canFly = canFly;
	}
}
