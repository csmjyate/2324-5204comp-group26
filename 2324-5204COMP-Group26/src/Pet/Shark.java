package Pet;

import java.time.LocalDateTime;
/**
 * class that deals with the shark object
 * @author judem
 *
 */
public class Shark extends Pets{
	private boolean canSwim;
	
	/**
	 * constructor for shark object
	 * @param petRef pet reference
	 * @param petName pet name
	 * @param speciesName species name
	 * @param regDate registration date
	 * @param breed breed of animal
	 * @param canSwim boolean for whether the animal can swim
	 */
	public Shark(String petRef, String petName, String speciesName, LocalDateTime regDate,String breed, boolean canSwim) {
		super(petRef, petName, speciesName, regDate, breed);
		this.canSwim = canSwim;
	}
	
	//Dont know if getters and setters are necessary but im making them for convenience
	/**
	 * getter for canSwim variable
	 * @return canSwim boolean that stores whether the animal can swim
	 */
	public boolean canItSwim() {
		return canSwim;
	}
	
	/**
	 * sets the canSwim variable
	 * @param canSwim boolean which stores whether the animal can swim
	 */
	public void setCanSwim(boolean canSwim) {
		this.canSwim = canSwim;
	}
	
}
