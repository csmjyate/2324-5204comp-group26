package Surgery;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import Booking.ManageBookings;
import main.Main;


/**
 * This is the class which stores all of the details about surgeries, including the get and set methods.
 * @author jacky
 *
 */
public class Surgeries {

//	private static final String PATH = "C:\\Users\\jacky\\OneDrive - Liverpool John Moores University\\csvFiles\surgery.csv";

	/** @author jacky
	 * We used serialisation of CSV files instead of serialisation using Ser files.
	 * This is the serialisation we would have performed if we to be using Ser.
	 */
	private final String PATH = "C:\\Users\\jacky\\OneDrive - Liverpool John Moores University\\Glynn Group Coursework\\data";
	
	private List<Surgeries> surgeries = new ArrayList<Surgeries>();

	private String surgeryName;
	private String postCode;
	private LocalTime openingTime;
	private LocalTime closingTime;
	private LocalDateTime trainingDays;

	/** @author jacky
	 * These are the attributes of a Surgery. All these parameters are to be passed
	 * into the array Surgeries and the get and set methods.
	 * 
	 * @param surgeryName
	 * @param postCode
	 * @param openingTime
	 * @param closingTime
	 * @param trainingDays
	 */
	public Surgeries(String surgeryName, String postCode, LocalTime openingTime, LocalTime closingTime,
			LocalDateTime trainingDays) {
		this.surgeryName = surgeryName;
		this.postCode = postCode;
		this.openingTime = openingTime;
		this.closingTime = closingTime;
		this.trainingDays = trainingDays;
	}
/**
 * The get method for surgery name.
 * @return
 */
	public String getSurgeryName() {
		return surgeryName;
	}
/**
 * The get method for surgery postcode.
 * @return
 */
	public String getPostcode() {
		return postCode;
	}

	/**
	 * The get method for surgery opening time.
	 * @return
	 */
	public LocalTime getOpeningTime() {
		return openingTime;
	}
/**
 * The get method for surgery closing time.
 * @return
 */
	public LocalTime getClosingTime() {
		return closingTime;
	}
/**
 * This gets the attribute for training days.
 * @return
 */
	public LocalDateTime getTrainingDays() {
		return trainingDays;
	}
/**
 * This sets the surgery name for Surgery.
 * @param surgeryName
 */
	public void setSurgeryName(String surgeryName) {
		this.surgeryName = surgeryName;
	}
/**
 * This sets the postcode attribute for the class Surgery. 
 * @param postCode
 */
	public void setPostCode(String postCode) {
		this.postCode = postCode;
	}

	/**
	 * This sets the opening time attribute for class Surgery.
	 * @param openingTime
	 */
	public void setOpeningTime(LocalTime openingTime) {
		this.openingTime = openingTime;
	}

	/**
	 * THis sets the closing time for the class Surgery.
	 * @param closingTime
	 */
	public void setClosingTime(LocalTime closingTime) {
		this.closingTime = closingTime;
	}

	/**
	 * This sets the training days attribute for the class Surgery.
	 * @param trainingDays
	 */
	public void setTrainingDays(LocalDateTime trainingDays) {
		this.trainingDays = trainingDays;
	}

	
	
	static final Scanner SCAN = new Scanner(System.in);

	/** @author jacky
	 * This is the deserialise method for the class Surgery which we would have
	 * used this if we hadn't used csv files. This method deserialises the surgery
	 * data for a user to see the data in reading format, utillising an array to do this.
	 */ 
	private void deSerializeSurgery() {
		ObjectInputStream ois;

		try {
			ois = new ObjectInputStream(new FileInputStream(PATH + "c&s.ser"));

			// NOTE : Erasure Warning !
			surgeries = (ArrayList<Surgeries>) ois.readObject();

			// ToDo : Finally ?
			ois.close();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	/** @author jacky
	 * This is the serialise method used to serialise the data of the Surgery class.
	 * When the data has been deserialised from a file, to store the data back in
	 * the file we need to serialise the data to store the data.
	 */
	private void serializeSurgery() {
		ObjectOutputStream oos;

		try {
			oos = new ObjectOutputStream(new FileOutputStream(PATH + "c&s.ser"));
			oos.writeObject(surgeries);

			// ToDo : Finally ?
			oos.close();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

}
