package Staff;

/**
 * Nurse class represents a nurse staff member in the system.
 * It inherits attributes such as staff reference, name, type of staff, number of bookings,
 * surgery ID, and number of surgeries.
 */

public class Nurse extends Staff {
	
	public Nurse (String staffRef, String name, String typeOfStaff, int noOfBookings, String surgeryID, int noOfSurgeries) {
		super(staffRef, name, typeOfStaff, noOfBookings, surgeryID, noOfSurgeries);
	}

	//making this void temporarily until we decide how this works
	public void consultingSurgeries() {
		System.out.println("Placeholder");
	}
}
