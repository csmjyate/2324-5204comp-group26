package Booking;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Reader;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;
import java.util.UUID;

import Staff.Staff;

/**
 * The ManageBookings class manages the overall bookings for the system which includes addition,
 * cancellation, searching, and viewing of the bookings.
 */

public class ManageBookings {
        // file's locations 

	 private static final String BOOKINGS_CSV_PATH = "C:\\Users\\jacky\\OneDrive - Liverpool John Moores University\\Glynn Group Coursework\\csvFiles\\bookings.csv";
	 private static final String STAFF_CSV_PATH = "C:\\Users\\jacky\\OneDrive - Liverpool John Moores University\\Glynn Group Coursework\\csvFiles\\staff.csv";
	 private static final String PETS_CSV_PATH = "C:\\Users\\jacky\\OneDrive - Liverpool John Moores University\\Glynn Group Coursework\\csvFiles\\pets.csv";
    private static final Scanner SCAN = new Scanner(System.in);  // using Scanner to get the user input

    private static Bookings currentBooking;

    public static void main(String[] args) {
    	ManageBookings manageBooking = new ManageBookings();
        manageBooking.deserializeAllBookings();
        manageBooking.bookingsMenu();
    }
    
    /**
     * Displays the menu hierarchy for facilitating console I/O.
     */
    
    public  void bookingsMenu() {
        String input = "";
        while (!input.equals("Q")) {
            System.out.println("---Bookings---");
            System.out.println("A. Add Bookings");
            System.out.println("B. Cancel Bookings");
            System.out.println("C. Search Bookings");
            System.out.println("D. View All Bookings");
            System.out.println("Q. Quit Bookings Menu");
            System.out.print("Please enter a menu selection: ");

            input = SCAN.next().toUpperCase();
            switch (input) {
                case "A" -> addBookings();
                case "B" -> cancelBookings();
                case "C" -> searchBookings();
                case "D" -> viewAllBookings();
                case "Q" -> quitBookingMenu();
                default -> System.out.printf("%nSorry! This input is not valid! Please try again!%n");
            }
        }
    }
	
    /**
     * Adds new booking according to the data entered by user for date, time, duration, staff ID, and pet ID.
     * Validates the IDs and checks for conflicting bookings.
     */
	public void addBookings() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter Bookings start date (DD/MM/YYYY): ");
        String startDateStr = scanner.nextLine();
        System.out.println("Enter Bookings start time (HH:MM): ");
        String startTimeStr = scanner.nextLine();

        try {
            // Parse start date and time
            LocalDateTime startDateTime = LocalDateTime.of(
                    LocalDate.parse(startDateStr, DateTimeFormatter.ofPattern("dd/MM/yyyy")),
                    LocalTime.parse(startTimeStr, DateTimeFormatter.ofPattern("HH:mm")));

            // Ask for duration
            System.out.println("Enter Bookings duration (hours): ");
            int hours = scanner.nextInt();
            System.out.println("Enter Bookings duration (minutes): ");
            int minutes = scanner.nextInt();
            

            // Calculate end date and time
            LocalDateTime endDateTime = startDateTime.plusHours(hours).plusMinutes(minutes);
            if(hasConflictingBooking(startDateTime, endDateTime)) {
            	System.out.println("There is already a booking in that time slot, please try again.");
            	return;
            }

            // Request staff ID and pet ID
            System.out.println("Enter Staff ID (3-digit reference number): ");
            String staffID = scanner.next();
            System.out.println("Enter Pet ID (5-digit reference number): ");
            String petID = scanner.next();

            // Validate staff ID and pet ID
            if (validateID(STAFF_CSV_PATH, staffID) && validateID(PETS_CSV_PATH, petID)) {
                // If both IDs are valid, proceed to create the booking
                UUID uuid = UUID.randomUUID();
                Bookings newBookings = new Bookings(uuid, staffID, endDateTime, startDateTime, petID);
                serializeBookings(newBookings); // Serialize after adding a Booking
                System.out.println("Bookings added successfully. Bookings ID: " + uuid);
            } else {
                System.out.println("Invalid staff ID or pet ID.");
                return;
            }
        } catch (DateTimeParseException | InputMismatchException e) {
            System.out.println(
                    "Invalid input format. Please enter dates in DD/MM/YYYY format, times in HH:MM format, and duration in hours and minutes as integers.");
        }
    }

    public boolean validateID(String filePath, String id) {
        try (BufferedReader br = new BufferedReader(new FileReader(filePath))) {
            String line;
            while ((line = br.readLine()) != null) {
                if (line.startsWith(id)) {
                    return true;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }


    public void viewAllBookings() {
    	ArrayList<Bookings> allBookings = deserializeAllBookings();
        System.out.println("All Current Bookings: ");
        for (Bookings bookings : allBookings) {
            System.out.println(bookings); // Print each bookings
        }
    }
    /**
     * Searches for a booking based on its specified starting date and time.
     */
    public void searchBookings() {
    	ArrayList<Bookings> allBookings = deserializeAllBookings();
    	Scanner scanner = new Scanner(System.in);
    	DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm");
        System.out.println("Enter the starting date and time of the Bookings you would like to search (dd/MM/yyyy HH:mm): ");
        String startTimeInput = scanner.nextLine();
        LocalDateTime startTime = LocalDateTime.parse(startTimeInput, formatter);
        boolean found = false;
        try {
            	for(Bookings booking : allBookings) {
            		if(booking.getStartDateTime().equals(startTime)) {
            			System.out.println("Booking Found!");
            			System.out.println(booking);
            			found = true;
            			break;
            		}
            	}
              if (!found) {
            	  System.out.println("There is no bookings with that starting time and date.");
              }
               
            
        } catch (IllegalArgumentException e) {
            System.out.println("Invalid start time format. Please enter a valid start time.");
        }
    }
    /**
     * Cancels a booking which matches the provided UUID.
     */
    public void cancelBookings() {
        System.out.println("Enter the UUID of the Bookings you would like to cancel: ");
        String BookingsIdStr = SCAN.nextLine();
        try {
            UUID BookingsId = UUID.fromString(BookingsIdStr);
            File file = new File(BOOKINGS_CSV_PATH + BookingsIdStr + ".dat");
            if (file.exists()) {
                file.delete();
                System.out.println("Bookings with ID " + BookingsId + " has been canceled.");
            } else {
                System.out.println("Bookings with ID " + BookingsId + " not found.");
            }
        } catch (IllegalArgumentException e) {
            System.out.println("Invalid UUID format. Please enter a valid UUID.");
        }
    }

   
    /**
     * Serializes the booking and saves it into the file.
     *
     * @param Bookings The booking is serialized and saved.
     */

    public void serializeBookings(Bookings Bookings) {
        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(BOOKINGS_CSV_PATH + Bookings.getId() + ".dat"))) {
            oos.writeObject(Bookings);
            System.out.println("Bookings serialized successfully.");
        } catch (IOException e) {
            System.err.println("Error serializing Bookings: " + e.getMessage());

        }
    }
    /**
     * Deserializes a booking from a file.
     *
     * @param file2 
     * @return The deserialized booking or null if booking not found.
     */
    public Bookings deserializeBookings(File file2) {
        String filePath = BOOKINGS_CSV_PATH + file2 + ".dat";
        File file = new File(filePath);
        if (!file.exists()) {
            return null;
        }
        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(filePath))) {
            return (Bookings) ois.readObject();
        } catch (IOException | ClassNotFoundException e) {
            System.err.println("Error deserializing Bookings: " + e.getMessage());
            return null;
        }
    }
     /**
     * Deserializes all the bookings.
     *
     * @return An ArrayList containing all the deserialized bookings.
     */
    public ArrayList<Bookings> deserializeAllBookings() {
        File folder = new File(BOOKINGS_CSV_PATH);
        File[] listOfFiles = folder.listFiles();
        ArrayList<Bookings> allBookings = new ArrayList<>();
        if (listOfFiles != null) {
            for (File file : listOfFiles) {
                if (file.isFile()) {
                    try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file))) {
                        Bookings bookings = deserializeBookings(file);
                        allBookings.add(bookings);
                    } catch (IOException e) {
                        System.err.println("Error deserializing Bookings: " + e.getMessage());
                    }
                }
            }
        }
        return allBookings;
    }
    

/**
 * overrides toString() method so that we can output our data the way we want
 */
    @Override
    public String toString() {
        if (currentBooking != null) {
            return "Booking ID: " + currentBooking.getId() +
                   "\nStart Date and Time: " + currentBooking.getStartDateTime() +
                   "\nEnd Date and Time: " + currentBooking.getEndDateTime();
        } else {
            return "No current booking.";
        }
    }
    
    /**
     * Quits the bookings menu and returns to main bookings menu.
     */
    public void quitBookingMenu() {
    	ManageBookings manageBooking = new ManageBookings();
    	System.out.println("   ");
    	System.out.println("   ");
    	System.out.println("   ");
    	System.out.println("   ");
    	System.out.println("   ");
    	System.out.println("   ");
    	System.out.println("   ");
    	System.out.println("   ");
    	System.out.println("   ");
    	System.out.println("   ");
    	System.out.println("   ");
    	System.out.println("   ");
    	System.out.println("   ");
    	manageBooking.bookingsMenu();
    }
    
     /**
     *  Checks for the conflicting bookings in the given time
     * @param startDateTime The start date and time of the booking.
     * @param endDateTime   The end date and time of the booking.
     * @return True if there is a conflicting booking, false otherwise.
     */
    public boolean hasConflictingBooking(LocalDateTime startDateTime, LocalDateTime endDateTime) {
    	 ArrayList<Bookings> allBookings = deserializeAllBookings();
        for (Bookings booking : allBookings) {
            if ((startDateTime.isAfter(booking.getStartDateTime()) && startDateTime.isBefore(booking.getEndDateTime())) ||
                (endDateTime.isAfter(booking.getStartDateTime()) && endDateTime.isBefore(booking.getEndDateTime())) ||
                (startDateTime.isBefore(booking.getStartDateTime()) && endDateTime.isAfter(booking.getEndDateTime())) ||
                (startDateTime.isEqual(booking.getStartDateTime()) || endDateTime.isEqual(booking.getEndDateTime()))) {
                return true; // Conflicting booking found
            }
        }
        return false; // No conflicting booking found
    }
}
   